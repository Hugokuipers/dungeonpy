import random

def main(attacker, defenders):
    for de in defenders:
        if de.boss:
            print(f"You can't escape {de.name}.")
            return False
    
    # count total stats, mult by luck, roll die
    a_stats = attacker.hp + attacker.strength + attacker.intelligence + attacker.defense + attacker.magic_defense + attacker.luck
    a_mult = 100 + (attacker.luck * 2)
    a_stats *= a_mult

    d_stats = 0
    for de in defenders:
        d_stat = de.hp + de.strength + de.intelligence + de.defense + de.magic_defense + de.luck
        d_mult = 100 + de.luck
        d_stat *= d_mult
        d_stats += d_stat
    
    total_stats = a_stats + d_stats
    escape_chance = random.randint(0, total_stats)

    # print(f"your stats: {a_stats}, enemy stats: {d_stats}, total stats: {total_stats}, escape chance: {escape_chance}")

    # if escape chance is smaller than your stat total you succeed
    if escape_chance < a_stats:
        print("\n\n~~You escaped succesfully.~~\n\n")
        return True

    print("You failed to run.")
    return False