import small_functions as sf
import library
import arsenal
import math

lib = library.library
items = arsenal.arsenal

def pick_target(enemies):
    livingEnemies = []
    for en in enemies:
        if en.hp > 0:
            livingEnemies.append(en)

    if len(livingEnemies) == 1:
        return livingEnemies[0]

    print(f"Pick a target: (1-{len(livingEnemies)}), B for back")
    for e in livingEnemies:
        print(f"{e.name} has {sf.getPercentage(e.hp, e.max_hp)}% health remaining.")
        
    answer = input()
    print("\n")
    if answer.isdigit():
        val = int(answer)
        if val > 0 and val <= len(livingEnemies):
            return livingEnemies[val-1]
    elif answer.lower() != 'b':
        print("Wrong input")

    return 0

def pick_skill(player):
    print(f"Pick a skill, or any other key to go back")
    for i, skill in enumerate(player.skills):
        print(f"{i+1}. {lib[skill].name} - cooldown: {player.skill_cd[skill]}")

    answer = input()

    if answer.isdigit():
        val = int(answer)
        if val > 0 and val <= len(player.skills):
            return player.skills[val-1]

    return 0

def pick_sale(intro, inv, multi, gold=9999999):
    print(intro)
    numsToAns = {}

    for i, item in enumerate(inv):
        print(f"{i+1}. {items[item].name} - Stock: {inv[item]} - Price: {math.ceil(items[item].gold_value * multi)}")
        numsToAns[i+1] = item

    answer = input()

    if answer.isdigit():
        val = int(answer)
        if val > 0 and val <= len(inv):
            if gold < math.ceil(items[numsToAns[val]].gold_value * multi):
                print("Not enough gold!")
                return 0
            return numsToAns[val]

    return 0

# pick option from array, values are displayed, values are returned
def pick_option(intro, options):
    print(intro)

    for i, opt in enumerate(options):
        print(f"{i+1}. {opt}")
    
    answer = input()
    print("\n")

    if answer.isdigit():
        val = int(answer)
        if val > 0 and val <= len(options):
            return options[val-1]
        
    print("Incorrect input. returning...")
    return 0

# pick option from dict, keys are displayed, and returned
def pick_option_from_dict(intro, options):
    print(intro)

    numsToAns = {}
    for i, opt in enumerate(options):
        print(f"{i+1}. {opt}")
        numsToAns[i] = opt
    
    answer = input()
    print("\n")

    if answer.isdigit():
        val = int(answer)
        if val > 0 and val <= len(options):
            return numsToAns[val-1]
        
    print("Incorrect input. returning...")
    return 0