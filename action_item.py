import arsenal

items = arsenal.arsenal

def main(user, item_name, enemies):
    item = items[item_name]

    if item.type == "heal":
        user.hp = min(user.hp + item.value, user.max_hp)
        print(f"{user.name} used {item.name} to heal {item.value} hp, {user.name} now has {user.hp} health")

        # remove 1 from stock, delete if empty, then return
        user.inventory[item_name] -= 1
        if user.inventory[item_name] == 0:
            del user.inventory[item_name]
            
        return True

    elif item.type == "damage":
        return True

    return False
    