import random
import math
import global_variables as gv
import small_functions as sf

def main(attacker, defender):
    print(f"{attacker.name} attacks {defender.name}.")
    if defender.hp == 0:
        print("Hitting a corpse is a somewhat disturbing habit, but to each their own.")

    damage = ((attacker.strength * attacker.strength) / defender.defense) * ((random.randint(-gv.ATT_VARIANCE,gv.ATT_VARIANCE) + 100) / 100)
    extra_message = ""

    if random.randint(0,99) < attacker.luck:
        damage *= 2
        extra_message = f"{attacker.name} landed a critical hit! "

    damage = math.ceil(damage)
    defender.hp = max(0, defender.hp-damage)
    print(f"{extra_message}{defender.name} got {damage} damage, {defender.name} has {sf.getPercentage(defender.hp, defender.max_hp)}% health remaining.")
