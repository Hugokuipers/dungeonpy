# Rewrite to tile struct
# 2 arrays

import random

# WorldMap class
#     int mapWidth    // MAX_MAP_WIDTH
#     int mapHeight   // MAX_MAP_HEIGHT
#     char map[MAX_MAP_HEIGHT][MAP_MAX_WIDTH]

# GenMap class
#     int mapWidth    // MAX_MAP_WIDTH
#     int mapHeight   // MAX_MAP_HEIGHT
#     char map[MAX_MAP_HEIGHT][MAP_MAX_WIDTH]

# WorldTile
#     int x
#     int y

# GenTile
#     int x
#     int y
#     bool visited

# tile class to add to working map:
class WorkTile:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.isStart = False
        self.visited = False
        self.clearing = False
        self.wall = True

# tile as it should be in worldMap
class WorldTile:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.char = "#"

# map for generating worldMap
class WorkMap:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.map = [] # in C Tile map[MAX_MAP_HEIGHT][MAP_MAX_WIDTH]
        self.start = []
        self.numClearings = 1 # this is per grid spot
        # grid width and height show the size of the grid ([6,6,6] || [3,4,3] || [5,4,5]), each piece will have a certain number of clearings, places that can be visited from start pos.
        self.gridWidth = []
        self.gridHeight = []

# map for actual use
class WorldMap:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.map = [] # in C (char of tile?) map[MAX_MAP_HEIGHT][MAP_MAX_WIDTH]


# myMap will be the final result map
myMap = WorldMap(30, 20)
# workingMap is the map in which information needed to create myMap will be stored
workingMap = WorkMap(30, 20)


# create both myMap and working map
def createMap():
    h = workingMap.height
    w = workingMap.width
    for i in range(h):
        row = []
        workingRow = []
        for j in range(w):
            row.append("#")
            workingRow.append(WorkTile(j,i))
            if j == 0 or i == 0 or i == workingMap.height-1 or j == workingMap.width-1:
                workingRow[j].visited = True
        myMap.map.append(row)
        workingMap.map.append(workingRow)

    x = random.randint(1, workingMap.width-2)
    y = random.randint(1, workingMap.height-2)
    workingMap.start = [x,y]
    myMap.map[y][x] = "@"
    workingMap.map[y][x].isStart = True
    workingMap.map[y][x].wall = False

# just print the map
def showMap():
    for i in range(myMap.height):
        print(" ".join(myMap.map[i]))


# devides the map in about 9 equal pieces, and calculates how many clearings (must visit spots) are neccesary based on map size
def createGrid():
    workingMap.numClearings = int((workingMap.width * workingMap.height) / 500)
    workingMap.numClearings += 1 if workingMap.numClearings == 0 else 0
    print(workingMap.numClearings)

    thirdWidth = int(workingMap.width/3)
    moduloWidth = workingMap.width % 3
    thirdHeight = int(workingMap.height/3)
    moduloHeight = workingMap.height % 3
    print(thirdHeight, moduloHeight, thirdWidth, moduloWidth)

    workingMap.gridWidth = [thirdWidth + (1 if moduloWidth == 2 else 0), thirdWidth + (1 if moduloWidth == 1 else 0), thirdWidth + (1 if moduloWidth == 2 else 0)]
    workingMap.gridHeight = [thirdHeight + (1 if moduloHeight == 2 else 0), thirdHeight + (1 if moduloHeight == 1 else 0), thirdHeight + (1 if moduloHeight == 2 else 0)]
    print(workingMap.gridWidth, workingMap.gridHeight)

# selects random spots up to the number of clearings, in each piece of the grid, to be visitable from startPos
def createClearings():
    #  si and sj are the differance in i and j based on the grid size
    si = 0
    for i in range(len(workingMap.gridHeight)):
        sj = 0
        for j in range(len(workingMap.gridWidth)):
            success = 0
            while success < workingMap.numClearings:
                x = random.randint(0, workingMap.gridWidth[j]-1)
                y = random.randint(0, workingMap.gridHeight[i]-1)

                if not (workingMap.map[y + si][x + sj].clearing == True or x + sj == 0 or y + si == 0 or x + sj >= workingMap.width-1 or y + si >= workingMap.height-1):
                    workingMap.map[y + si][x + sj].clearing = True
                    success += 1

            sj += workingMap.gridWidth[j]
        si += workingMap.gridHeight[i]


#  create a path from start to every clearing, randomly cutting away walls until all are reachable (TODO this became so short, not sure if still needed)
def createPath():
    print(workingMap.start)
    found = randomlyWalk(workingMap.start[0], workingMap.start[1])
    print(found)
    showMap()

#  recursive func used in createPath
def randomlyWalk(x, y):
    if workingMap.map[y][x].visited == True:
        return False
    
    # set current as visited
    workingMap.map[y][x].visited = True

    neigh = [[-1,0],[1,0],[0,-1],[0,1]]
    foundClear = False

    # check if this is a clearing
    if workingMap.map[y][x].clearing == True:
        foundClear = True

    while len(neigh) > 0:
        index = random.randint(0, len(neigh) - 1)
        n = neigh.pop(index)
        foundClear = randomlyWalk(x+n[0], y+n[1]) or foundClear
    
    # if this, or somewhere we can go from here is a clearing, make this a not wall spot
    if foundClear:
        myMap.map[y][x] = ' '
        workingMap.map[y][x].wall = False
    
    return foundClear




createMap()

showMap()

createGrid()

clearings = createClearings()

createPath()

showMap()


