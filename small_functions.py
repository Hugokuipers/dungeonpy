import random
import math

def getPercentage(val, total):
    if val <= 0:
        return 0
    return math.ceil(100 / (total / val))

# change a number by a percentage
def applyVariance(val, var, rond=False):
    var *= 100
    var = random.randint(-var, var) + 10000
    val *= var
    val /= 10000

    if rond:
        val = round(val)

    return val