import random
import math
import library
import arsenal
import player_interaction as pi
import json

lib = library.library
items = arsenal.arsenal

# player class:
class Entity:
    def __init__(self, name, hp, strength, intelligence, defense, magic_defense, luck):
        self.name = name
        self.level = 1
        self.hp = hp
        self.max_hp = hp
        self.strength = strength
        self.base_strength = strength
        self.intelligence = intelligence
        self.base_intelligence = intelligence
        self.defense = defense
        self.base_defense = defense
        self.magic_defense = magic_defense
        self.base_magic_defense = magic_defense
        self.luck = luck
        self.base_luck = luck
        self.skills = []
        self.skill_cd = {}

class Player(Entity):
    def __init__(self, name, hp, stre, inte, defe, mg_def, luck):
        super().__init__(name, hp, stre, inte, defe, mg_def, luck)
        self.gold = 0
        self.exp = 0
        self.inventory = {}
        self.location = 1
    
    def checkLvlUp(self):
        kwa = self.level**2
        prev = (self.level - 1)**2
        needed = (kwa - prev) * 100

        if self.exp >= needed:
            self.lvlUp(self.exp - needed)
        return False

    def lvlUp(self, extraExp):
        self.exp = extraExp
        self.level += 1

        print("You have leveled up!")
        print("All your stats have increased.\n")
        options = {"Hp": "hp", "Strength": "strength", "Intelligence": "intelligence", "Defense": "defense", "Magic Defense": "magic_defense", "Luck": "luck"}
        for stat in options:
            if stat == "Hp":
                self.increaseStat(options[stat],10)
                continue
            self.increaseStat(options[stat],1)

        self.displayStats()

        increases = 0
        while increases < self.level * 2:
            answer = pi.pick_option_from_dict("\nPlease pick a stat to increase:", options)
            if answer == 0: continue

            if options[answer] == "hp":
                self.increaseStat(options[answer],10)
            else:
                self.increaseStat(options[answer],1)
                
            print(f"Increased {answer}")
            increases += 1

        self.displayStats()

        if self.level % 2 == 0:
            skills = self.getAllLearableSkills()
            while True:
                answer = pi.pick_option_from_dict("You can learn a skill:", skills)
                if answer == 0: continue

                print(f"You learned {answer}")
                self.skills.append(skills[answer])
                break
    
    def save(self):
        saveData = {
            "name": self.name,
            "hp": self.max_hp,
            "strength": self.base_strength,
            "intelligence": self.base_intelligence,
            "defense": self.base_defense,
            "magic_defense": self.base_magic_defense,
            "luck": self.base_luck,
            "level": self.level,
            "gold": self.gold,
            "exp": self.exp,
            "location": self.location,
            "inventory": {},
            "skills": []
        }

        for item in self.inventory:
            saveData["inventory"][item] = self.inventory[item]
        
        for skill in self.skills:
            saveData["skills"].append(skill)
        
        with open('save_data.JSON', 'w') as f:
            json.dump(saveData, f)

        return

    def load(self):
        with open('save_data.JSON') as f:
            saveData = json.load(f)
            self.name = saveData["name"]
            self.level = saveData["level"]
            self.hp = saveData["hp"]
            self.max_hp = saveData["hp"]
            self.strength = saveData["strength"]
            self.base_strength = saveData["strength"]
            self.intelligence = saveData["intelligence"]
            self.base_intelligence = saveData["intelligence"]
            self.defense = saveData["defense"]
            self.base_defense = saveData["defense"]
            self.magic_defense = saveData["magic_defense"]
            self.base_magic_defense = saveData["magic_defense"]
            self.luck = saveData["luck"]
            self.base_luck = saveData["luck"]
            self.skills = []
            self.skill_cd = {}
            self.gold = saveData["gold"]
            self.exp = saveData["exp"]
            self.inventory = {}
            self.location = saveData["location"]

            for item in saveData["inventory"]:
                self.inventory[item] = saveData["inventory"][item]
            
            for skill in saveData["skills"]:
                self.skills.append(skill)
    
    def displayStats(self,base=True):
        print(f"You are level {self.level}")

        if base:
            print("Current base stats:")
            print(f"Hp:             {self.hp} / {self.max_hp}")
            print(f"Strength:       {self.base_strength}")
            print(f"Intelligence:   {self.base_intelligence}")
            print(f"Defense:        {self.base_defense}")
            print(f"Magic Defense:  {self.base_magic_defense}")
            print(f"Luck:           {self.base_luck}")
        else:
            print("Current stats:")
            print(f"Hp:             {self.hp} / {self.max_hp}")
            print(f"Strength:       {self.strength}")
            print(f"Intelligence:   {self.intelligence}")
            print(f"Defense:        {self.defense}")
            print(f"Magic Defense:  {self.magic_defense}")
            print(f"Luck:           {self.luck}")

        print(f"Gold:           {self.gold}")
        print(f"Exp:            {self.exp}")
        print("\n")

    def displayInventory(self):
        print("You have in your inventory:")
        for item in self.inventory:
            print(f"{self.inventory[item]} {items[item].name}")
        print("\n")
        
    def getAllLearableSkills(self):
        skills = {}

        for skill in lib:
            if skill in self.skills:
                continue
            
            if not lib[skill].prerequisite == "none":
                learnable = True

                for sk in lib[skill].prerequisite.split(":"):
                    if not sk in self.skills:
                        learnable = False
                        break
                if not learnable:
                    continue
                    
            skills[lib[skill].name] = skill
        
        return skills
    
    def increaseStat(self, stat, amount, temp=False):
        
        if stat == "hp":
            if not temp:
                self.max_hp += amount
            self.hp += amount
        elif stat == "strength":
            if not temp:
                self.base_strength += amount
            self.strength += amount
        elif stat == "intelligence":
            if not temp:
                self.base_intelligence += amount
            self.intelligence += amount
        elif stat == "defense":
            if not temp:
                self.base_defense += amount
            self.defense += amount
        elif stat == "magic_defense":
            if not temp:
                self.base_magic_defense += amount
            self.magic_defense += amount
        elif stat == "luck":
            if not temp:
                self.base_luck += amount
            self.luck += amount
            




# enemy class:
class Enemy(Entity):
    def __init__(self, name, hp, stre, inte, defe, mg_def, luck, pers, gold, exp, boss=0):
        super().__init__(name, hp, stre, inte, defe, mg_def, luck)
        self.boss = boss
        self.personality = pers
        self.dropped_gold = gold
        self.exp_reward = exp
        self.drop_table = {}

