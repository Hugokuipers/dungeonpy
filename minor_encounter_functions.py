import random
import arsenal

items = arsenal.arsenal

def check_end_conditions(player, enemies):
    if player.hp <= 0:
        print("\n\n~~Please restart at the nearest bonfire.~~\n\n")
        return 0

    for e in enemies:
        if e.hp > 0:
            return 1

    print("\n\n~~Gratz, have some internet cookies.~~\n\n")
    return 2


def decreaseSkillCds(entity):
    # if entity did a normal attack, or failed to cast a skill, decrease cooldowns
    for skill in entity.skill_cd:
        if entity.skill_cd[skill] > 0:
            entity.skill_cd[skill] -= 1


def spoilsOfWar(player, enemies):
    gold_spoils = 0
    exp_spoils = 0
    item_spoils = {}

    for en in enemies:
        gold_spoils += en.dropped_gold
        exp_spoils += en.exp_reward

        randomlySelected = random.randint(0,99)
        currentNum = 0
        for drop in en.drop_table:
            currentNum += en.drop_table[drop]
            if randomlySelected < currentNum:
                if drop == "empty":
                    break
                
                if drop not in item_spoils:
                    item_spoils[drop] = 0
                item_spoils[drop] += 1
                break

    player.gold += gold_spoils
    player.exp += exp_spoils

    extra = ""
    for item in item_spoils:
        if item not in player.inventory:
            player.inventory[item] = 0
        player.inventory[item] += item_spoils[item]

        extra = f"{extra} and {item_spoils[item]} {items[item].name}"

    print(f"You obtained {gold_spoils} gold{extra}.")
    return
