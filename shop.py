import player_interaction as pi
import arsenal as arsenal
import math

items = arsenal.arsenal

def main(player):
    # TODO do this via a class in the main game probably??
    buy_multiplier = 2.0
    sell_multiplier = 1.0
    inventory = {
        "potion":           5,
        "better_potion":    2
    }

    # the further you are, the friendlier the merchant, and the better the stock
    if player.location >= 15:
        buy_multiplier -= 0.1
        sell_multiplier += 0.1
        inventory["potion"] += 15
        inventory["better_potion"] += 3

    while True:
        print(f"\nYou have {player.gold} gold.")
        answer = pi.pick_option("Would you like to sell or buy, any other key to exit the shop:", ["Sell", "Buy"])

        if answer == "Sell":
            sell(player, sell_multiplier)
        elif answer == "Buy":
            buy(player, buy_multiplier, inventory)
        else:
            print("Now back to the good part\n")
            break
    
def sell(player, sell_multi):
    while True:
        answer = pi.pick_sale("What would you like to sell? (Any other key to return):", player.inventory, sell_multi)

        if answer == 0:
            return
        
        player.inventory[answer] -= 1
        if player.inventory[answer] == 0:
            del player.inventory[answer]
        
        player.gold += math.ceil(items[answer].gold_value * sell_multi)
        print(f"\nYou have {player.gold} gold.\n")

def buy(player, buy_multi, inv):
    while True:
        answer = pi.pick_sale("What would you like to buy? (Any other key to return):", inv, buy_multi, player.gold)

        if answer == 0:
            return
        
        if answer not in player.inventory:
            player.inventory[answer] = 0    
        player.inventory[answer] += 1
        
        player.gold -= math.ceil(items[answer].gold_value * buy_multi)
        print(f"\nYou have {player.gold} gold.\n")