import random
import entities
import name_gen
import global_variables as gv
import small_functions as sf

# a class for every enemy type i guess:

# Slime is well... a slime
class Slime(entities.Enemy):
    def __init__(self):
        personality = "std"
        dropped_gold = sf.applyVariance(8, gv.REWARD_VARIANCE, True)
        exp_reward = 50
        
        hp = sf.applyVariance(40, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(8, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(2, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(13, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(4, gv.STAT_VARIANCE, True)
        luck = 0
        super().__init__("Slime", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward)

        self.skills.append("dissolve")

        self.drop_table = { "empty":      40,
                            "potion":     20,
                            "slime_core": 40  
                          }


# Skeleton is a walking corspe, without the fleshy bits, high att, brittle
class Skeleton(entities.Enemy):
    def __init__(self):
        personality = "std"
        dropped_gold = sf.applyVariance(10, gv.REWARD_VARIANCE, True)
        exp_reward = 80
        
        hp = sf.applyVariance(60, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(18, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(5, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(7, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(8, gv.STAT_VARIANCE, True)
        luck = sf.applyVariance(5, gv.STAT_VARIANCE, True)
        super().__init__("Skeleton", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward)

        self.skills.append("heavy_strike")

        self.drop_table = { "empty":        30,
                            "potion":       20,
                            "cracked_bone": 40,
                            "intact_skull": 10
                          }

# Naga y'know
class Naga(entities.Enemy):
    def __init__(self):
        personality = "std"
        dropped_gold = sf.applyVariance(20, gv.REWARD_VARIANCE, True)
        exp_reward = 200
        
        hp = sf.applyVariance(160, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(16, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(17, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(18, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(14, gv.STAT_VARIANCE, True)
        luck = sf.applyVariance(10, gv.STAT_VARIANCE, True)
        super().__init__("Naga", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward)

        self.skills.append("heavy_strike")
        self.skills.append("naga_embrace")

        self.drop_table = { "empty":        15,
                            "potion":       25,
                            "better_potion":20,
                            "naga_tail":    30,
                            "dark_amulet":  10
                          }

# Goblin
class Goblin(entities.Enemy):
    def __init__(self):
        personality = "std"
        dropped_gold = sf.applyVariance(10, gv.REWARD_VARIANCE, True)
        exp_reward = 100
        
        hp = sf.applyVariance(140, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(10, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(6, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(20, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(16, gv.STAT_VARIANCE, True)
        luck = sf.applyVariance(2, gv.STAT_VARIANCE, True)
        super().__init__("Goblin", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward)

        self.skills.append("strike")
        # self.skills.append("stone_skin")

        self.drop_table = { "empty":        30,
                            "potion":       20,
                            "goblin_ear":   40,
                            "stone_amulet": 10
                          }

# Goblin mage (first mage creature)
class GoblinMage(entities.Enemy):
    def __init__(self):
        personality = "concentrating"
        dropped_gold = sf.applyVariance(15, gv.REWARD_VARIANCE, True)
        exp_reward = 120
        
        hp = sf.applyVariance(70, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(4, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(20, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(6, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(14, gv.STAT_VARIANCE, True)
        luck = sf.applyVariance(8, gv.STAT_VARIANCE, True)
        super().__init__("Goblin Mage", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward)

        self.skills.append("fireball")
        self.skills.append("fire_whip")
        self.skills.append("concentrate")

        self.drop_table = { "empty":        20,
                            "potion":       30,
                            "better_potion":10,
                            "goblin_ear":   40
                          }

# Goblin champion (first boss creature)
class GoblinChampion(entities.Enemy):
    def __init__(self):
        personality = "skilled"
        dropped_gold = sf.applyVariance(100, gv.REWARD_VARIANCE, True)
        exp_reward = 1000
        
        hp = sf.applyVariance(1000, gv.STAT_VARIANCE, True)
        stre = sf.applyVariance(30, gv.STAT_VARIANCE, True)
        inte = sf.applyVariance(20, gv.STAT_VARIANCE, True)
        defe = sf.applyVariance(36, gv.STAT_VARIANCE, True)
        mg_def = sf.applyVariance(26, gv.STAT_VARIANCE, True)
        luck = sf.applyVariance(5, gv.STAT_VARIANCE, True)
        super().__init__(f"Goblin Champion {name_gen.getRandomNameFromList()}", hp, stre, inte, defe, mg_def, luck, personality, dropped_gold, exp_reward, 1)

        self.skills.append("fire_whip")
        self.skills.append("explosion")

        self.drop_table = { "head_of_the_goblin_champion": 100
                          }