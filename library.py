import random

# skills to be used by player or enemy:
class Skill:
    def __init__(self, name, type, sub_type, base_dam, base_crit_ch, base_crit_mult, cd, area, prerequisite="none"):
        self.name = name
        self.type = type
        self.sub_type = sub_type
        self.base_dam = base_dam
        self.base_crit_ch = base_crit_ch
        self.base_crit_mult = base_crit_mult
        self.cd = cd
        self.area = area
        self.prerequisite = prerequisite

# library will hold all skills
library = {}

library["fireball"] = Skill(
    "Fireball",
    "damage",
    "magic",
    16,
    10,
    2,
    1,
    0
)

library["fire_whip"] = Skill(
    "Fire Whip",
    "damage",
    "magic",
    32,
    10,
    2,
    2,
    0,
    "fireball"
)

library["inferno"] = Skill(
    "Inferno",
    "damage",
    "magic",
    48,
    10,
    2,
    3,
    0,
    "fire_whip"
)

library["flail_wildly"] = Skill(
    "Flail Wildly",
    "damage",
    "physical",
    5,
    5,
    10,
    0,
    0
)

library["backstab"] = Skill(
    "Backstab",
    "damage",
    "physical",
    12,
    25,
    3,
    1,
    0
)

library["dissolve"] = Skill(
    "Dissolve",
    "damage",
    "physical",
    12,
    10,
    2,
    2,
    0,
    "monster_physique"
)

library["strike"] = Skill(
    "Strike",
    "damage",
    "physical",
    15,
    10,
    2,
    2,
    0
)

library["heavy_strike"] = Skill(
    "Heavy Strike",
    "damage",
    "physical",
    30,
    10,
    2,
    2,
    0,
    "strike"
)

library["frost_fists"] = Skill(
    "Frost Fists",
    "damage",
    "hybrid",
    30,
    10,
    2,
    3,
    0
)

library["explosion"] = Skill(
    "Explosion",
    "damage",
    "hybrid",
    40,
    -10,
    2,
    4,
    1,
    "fireball:frost_fists"
)

library["naga_embrace"] = Skill(
    "Naga's Embrace",
    "damage",
    "hybrid",
    30,
    30,
    3,
    4,
    0,
    "monster_physique"
)

library["stone_skin"] = Skill(
    "Stone Skin",
    "buff",
    "defense",
    20,
    0,
    0,
    1,
    0,
    "currently_unavailable"
)

library["stone_army"] = Skill(
    "Stone Army",
    "buff",
    "defense",
    20,
    0,
    0,
    3,
    1,
    "stone_skin"
)

library["steel_skin"] = Skill(
    "Steel Skin",
    "buff",
    "defense",
    35,
    0,
    0,
    2,
    0,
    "stone_skin"
)

library["steel_army"] = Skill(
    "Steel Army",
    "buff",
    "defense",
    35,
    0,
    0,
    4,
    1,
    "steel_skin"
)

library["inspect"] = Skill(
    "Inspect",
    "special",
    "inspect",
    0,
    0,
    0,
    0,
    0,
    "currently_unavailable"
)

library["concentrate"] = Skill(
    "Concentrate",
    "special",
    "concentrate",
    2,
    0,
    0,
    0,
    0
)

library["focus"] = Skill(
    "Focus",
    "special",
    "concentrate",
    3,
    0,
    0,
    0,
    0,
    "concentrate"
)

library["meditate"] = Skill(
    "Meditate",
    "special",
    "meditate",
    10,
    0,
    0,
    0,
    0,
    "currently_unavailable"
)