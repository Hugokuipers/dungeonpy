import random
import action_cast
import action_attack
import library
import minor_encounter_functions as mef

action_cast = action_cast.main
action_attack = action_attack.main
lib = library.library

def std_enemy_turn(pl, en, all_enemies):
    # either use a skill or regular attack (pick randomly, might fail)
    if random.randint(0, 99) < 20 and len(en.skills) > 0:
        skill = en.skills[random.randint(0, len(en.skills)-1)]
        if en.skill_cd[skill] == 0:
            action_cast(en, pl, skill)
            en.skill_cd[skill] = lib[skill].cd
            return

        print(f"{en.name} fails to cast {lib[skill].name}.")
    else:
        action_attack(en, pl)

    mef.decreaseSkillCds(en)
    return
    

def skilled_enemy_turn(pl, en, all_enemies):
    useableSkills = [sk for sk in en.skills if en.skill_cd[sk] == 0]
    # for sk in en.skills:
    #     if en.skill_cd[sk] == 0:
    #         useableSkills.append(sk)

    # if available use a random available skill:
    if len(useableSkills) > 0:
        sk = useableSkills[random.randint(0, len(useableSkills)-1)]
        action_cast(en, pl, sk)
        en.skill_cd[sk] = lib[sk].cd
        return
    else:
        # use regular attack instead
        action_attack(en, pl)
        mef.decreaseSkillCds(en)    


def concentrating_enemy_turn(pl, en, all_enemies):
    useableSkills = []
    for sk in en.skills:
        print(en.skill_cd[sk])
        if en.skill_cd[sk] == 0 and lib[sk].type != "special":
            useableSkills.append(sk)

    # if available use a random available skill:
    if len(useableSkills) > 0:
        sk = useableSkills[random.randint(0, len(useableSkills)-1)]
        action_cast(en, pl, sk)
        en.skill_cd[sk] = lib[sk].cd
        return
    else:
        # concentrate on one of it's skills
        longestCd = en.skill_cd[en.skills[0]]
        longestCdSkill = en.skills[0]
        concenSkill = None
        print(concenSkill)
        for sk in en.skills:
            if lib[sk].sub_type == "concentrate":
                concenSkill = sk
                break
            elif lib[sk].type != "special" :
                cd = en.skill_cd[sk]
                if cd > longestCd:
                    longestCd = cd
                    longestCdSkill = sk
    
        action_cast(en, pl, concenSkill, [longestCdSkill])
