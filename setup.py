# copied code to make .exe file

from cx_Freeze import setup, Executable

base = None    

executables = [Executable("encounter.py", base=base)]

packages = ["idna", "os", "random", "math", "library", "arsenal", "name_gen", "entities", "monsterpedia", "personalities", "global_variables", "small_functions", "minor_encounter_functions", "player_interaction", "action_cast", "action_attack", "action_run", "action_item", "shop", "json"]
options = {
    'build_exe': {    
        'packages':packages,
    },    
}

setup(
    name = "Encounter",
    options = options,
    version = "0.0.1",
    description = 'Python version of a system that will be part of final C game',
    executables = executables
)