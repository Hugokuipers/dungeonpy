import library
import random
import math
import minor_encounter_functions as mef
import global_variables as gv
import small_functions as sf

lib = library.library

def main(attacker, defender, skill, opt=[]):
    if attacker.skill_cd[skill] != 0:
        return False

    sk = lib[skill]

    # go to skill type function:
    if sk.type == "damage":
        # damage_cast()
        pass
    elif sk.type == "buff":
        # buff_cast()
        pass
    elif sk.type == "special":
        return special_cast(attacker, defender, skill, opt)

    print(f"{attacker.name} casts {sk.name} on {defender.name}.")
    if defender.hp == 0:
        print("Hitting a corpse is a somewhat disturbing habit, but to each their own.")
    
    tot_att = sk.base_dam
    tot_def = 1
    tot_crit = attacker.luck + sk.base_crit_ch
    extra_message = ""

    if sk.sub_type == "physical" or sk.sub_type == "hybrid":
        tot_att *= attacker.strength
        tot_def *= defender.defense
    if sk.sub_type == "magic" or sk.sub_type == "hybrid":
        tot_att *= attacker.intelligence
        tot_def *= defender.magic_defense

    damage = (tot_att / tot_def) * ((random.randint(-gv.ATT_VARIANCE,gv.ATT_VARIANCE) + 100) / 100)

    if random.randint(0,99) < tot_crit:
        damage *= sk.base_crit_mult
        extra_message = f"{attacker.name} landed a critical hit! "
    
    damage = math.ceil(damage)
    defender.hp = max(0, defender.hp-damage)
    print(f"{extra_message}{defender.name} got {damage} damage, {defender.name} has {sf.getPercentage(defender.hp, defender.max_hp)}% health remaining.")
    attacker.skill_cd[skill] = lib[skill].cd
    
    return True

def special_cast(attacker, defender, skill, opt):
    sk = lib[skill]
    if sk.sub_type == "concentrate":
        return special_concentrate(attacker, skill, opt[0])
    elif sk.sub_type == "inspect":
        return special_inspect(attacker, defender, skill)
    elif sk.sub_type == "meditate":
        return special_meditate(attacker, skill)

def special_concentrate(attacker, skill, secondSkill):
    sk = lib[skill]
    print(f"{attacker.name} concentrates on {lib[secondSkill].name}")

    # decrease cd of every skill by one, and 1 by more
    mef.decreaseSkillCds(attacker)
    if attacker.skill_cd[secondSkill] > sk.base_dam-1:
        attacker.skill_cd[secondSkill] -= sk.base_dam-1
    else:
        attacker.skill_cd[secondSkill] = 0            

    return True


def special_inspect(attacker, defender, skill):
    pass

def special_meditate(attacker, skill):
    pass
