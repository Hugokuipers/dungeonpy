import random

# items to be used by player or carried by enemy (value is based on type):
class Item:
    def __init__(self, name, type, value, gold_value):
        self.name = name
        self.type = type
        self.value = value
        self.gold_value = gold_value

# arsenal will hold all itmes
arsenal = {}

arsenal["potion"] = Item(
    "Potion",
    "heal",
    40,
    25
)

arsenal["better_potion"] = Item(
    "Better Potion",
    "heal",
    100,
    60
)

arsenal["dark_amulet"] = Item(
    "Dark Amulet",
    "debuff:curse",
    2,
    35
)

arsenal["stone_amulet"] = Item(
    "Stone Amulet",
    "buff:defense",
    20,
    20
)

arsenal["slime_core"] = Item(
    "Slime Core",
    "trash",
    8,
    8
)

arsenal["cracked_bone"] = Item(
    "Cracked Bone",
    "trash",
    6,
    6
)

arsenal["intact_skull"] = Item(
    "Intact Skull",
    "trash",
    25,
    25
)

arsenal["naga_tail"] = Item(
    "Naga Tail",
    "trash",
    18,
    18
)

arsenal["goblin_ear"] = Item(
    "Goblin Ear",
    "trash",
    10,
    10
)

arsenal["head_of_the_goblin_champion"] = Item(
    "Head of the Goblin Champion",
    "trophy",
    250,
    250
)
