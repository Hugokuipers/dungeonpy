import random
import os
import math
import library
import arsenal
import name_gen
import entities
import monsterpedia
import personalities
import global_variables as gv
import small_functions as sf
import minor_encounter_functions as mef
import player_interaction as pi
import action_cast
import action_attack
import action_run
import action_item
import shop

action_cast = action_cast.main
action_attack = action_attack.main
action_run = action_run.main
action_item = action_item.main

# library will hold all skills
lib = library.library

# arsenal will hold all itmes
items = arsenal.arsenal

# call main once etc
def main():
    player = None

    if os.path.exists('save_data.JSON'):
        load = pi.pick_option("Would you like to load a save game?", ["Yes", "No"])
        if load == "Yes":
            player = load_player()

    if player == None:
        player = create_player()

    # this is the main game loop:
    exit = False
    while not exit: 
        exit = generateFight(player)
        if exit:
            break
        exit = progressChoice(player)

    print("\n\n\nThe game is now over :)\n\n\n")

    restart = pi.pick_option("Would you like to restart the game?", ["Yes", "No"])

    if restart == "Yes":
        main()


# create a new player obj
# TODO moet obviously in player file, maar for now is dit makkelijker testen
def create_player():
    print("Please enter your name:")
    name = input()
    if len(name) < 2: name = "Harry"

    pl = entities.Player(name, 100, 10, 10, 10, 10, 10)

    return pl

def load_player():
    pl = entities.Player("Saved_name", 100, 10, 10, 10, 10, 10)

    pl.load()

    return pl

# fight between player and enemy:
def fight(player, enemies):
    # set cooldown for all skills active
    player.skill_cd = {s:lib[s].cd for s in player.skills}
    
    for e in enemies:
        e.skill_cd = {s:lib[s].cd for s in e.skills}
    
    print(f"\nYou are fighting {', '.join([e.name for e in enemies])}")
    
    endCode = 1
    while endCode == 1:
        endCode = player_turn(player, enemies)
        if endCode != 1: break

        for e in enemies:
            if e.hp <= 0: continue

            enemy_turn(player, e, enemies)
            
            endCode = mef.check_end_conditions(player, enemies)
            if endCode != 1: 
                break
    
    if endCode == 2:
        mef.spoilsOfWar(player, enemies)
        player.checkLvlUp()
    
    print("\n")

def player_turn(player, enemies):
    while True:
        answer = pi.pick_option("\nWhat will you do?", ["Attack", "Use Skill", "Use Item", "Run", "Stats"])
        target = enemies[0]
        skill = ""
        
        if answer == "Attack":
            # TODO :: create attack function
            target = pi.pick_target(enemies)

            if target == 0:
                continue
            
            action_attack(player, target)
            mef.decreaseSkillCds(player)
            break

        elif answer == "Use Skill":
            # TODO :: create skill function
            if len(player.skills) < 1:
                print("You don't have any skills, you noob")
                continue

            skill = pi.pick_skill(player)

            if skill == 0:
                continue
            
            target = None
            opt = []
            if lib[skill].type == "damage":
                target = pi.pick_target(enemies)
            elif lib[skill].type == "buff":
                target = pi.pick_target([player].extend(enemies))
            elif lib[skill].type == "special":
                if lib[skill].sub_type == "concentrate":
                    answer = pi.pick_option("On which skill do you want to concentrate?", player.skills)
                    if answer == 0:
                        continue
                    opt.append(answer)

            if target == 0:
                continue
            
            if not action_cast(player, target, skill, opt):
                print(f"{player.name} fails to cast {lib[skill].name}.")
                mef.decreaseSkillCds(player)

            break
        
        elif answer == "Use Item":
            # TODO :: create item function
            if len(player.inventory) < 1:
                print("You don't have any items, you peasant")
                continue
            
            choosingDict = {}
            for i, item in enumerate(player.inventory):
                print(f"{i+1}. {items[item].name} - stock: {player.inventory[item]}")
                choosingDict[i+1] = item


            print(f"Pick an item, or B for back")

            # TODO :: pick answer can be a function, it's the same as with the skill
            answer = input()

            if answer.lower() == 'b':
                continue
            elif not answer.isdigit():
                print("Wrong input")
                continue

            val = int(answer)
            if val < 1 or val > len(player.inventory):
                print("Wrong input")
                continue

            item = choosingDict[val]

            if not action_item(player, item, enemies):
                continue
            
            break

        elif answer == "Run":
            if action_run(player, enemies):
                return 3
            break
        
        elif answer == "Stats":
            player.displayStats(False)
            player.displayInventory()
            continue

        print("Wrong input")
        continue

    return mef.check_end_conditions(player, enemies)

def enemy_turn(pl, en, all_enemies):
    print(f"\n{en.name}'s turn")
    if en.personality == "std":
        personalities.std_enemy_turn(pl, en, all_enemies)
    
    elif en.personality == "skilled":
        personalities.skilled_enemy_turn(pl, en, all_enemies)

    elif en.personality == "concentrating":
        personalities.concentrating_enemy_turn(pl, en, all_enemies)

def progressChoice(player):
    answer = ""
    if player.location == 1:
        answer = pi.pick_option("Advance further into the dungeon, rest at home (restore hp and save), or stay in the same area:", ["Advance", "Rest", "Stay"])
    elif player.location == 5:
        answer = pi.pick_option("Advance further into the dungeon, retreat towards the entrance, stay in the same area, or visit the shop:", ["Advance", "Retreat", "Stay", "Shop"])
    else:
        answer = pi.pick_option("Advance further into the dungeon, retreat towards the entrance, or stay in the same area:", ["Advance", "Retreat", "Stay"])

    if answer == "Advance":
        player.location += 1
        print(f"You are now in location {player.location}.\n")
    elif answer == "Retreat":
        player.location -= 1
        print(f"You are now in location {player.location}.\n")
    elif answer == "Rest":
        player.hp = player.max_hp
        player.save()
        print(f"You are well rested and ready to murder some more slimes.\n")
    elif answer == "Shop":
        shop.main(player)

    return True if player.location > 10 else False

def generateFight(player):
    monsters = []

    # generate a fight based on how deep in the dungeon the player is
    # TODO give every monster a threath level, dynamically create matchups based on a total threath level, with min, max, en total enemy count
     # TODO obviously this needs to go somewhere 
    if player.location == 1:
        mon = monsterpedia.Slime()
        monsters.append(mon)

    elif player.location == 2:
        randomNum = random.randint(1,2)
        if randomNum == 1:
            mon = monsterpedia.Slime()
            monsters.append(mon)
        mon = monsterpedia.Slime()
        monsters.append(mon)

    elif player.location == 3:
        randomNum = random.randint(1,6)
        if randomNum < 6:
            mon = monsterpedia.Slime()
            mon2 = monsterpedia.Slime()
            monsters.extend([mon, mon2])
        elif randomNum == 6:
            mon = monsterpedia.Skeleton()
            monsters.append(mon)
        if randomNum == 5:
            mon = monsterpedia.Slime()
            monsters.append(mon)
    
    elif player.location == 4:
        randomNum = random.randint(1,8)
        if randomNum < 7:
            mon = monsterpedia.Skeleton()
            monsters.append(mon)
        else:
            mon = monsterpedia.Slime()
            mon2 = monsterpedia.Slime()
            monsters.extend([mon, mon2])
            if randomNum == 8:
                mon3 = monsterpedia.Slime()
                monsters.append(mon3)

    elif player.location == 5:
        randomNum = random.randint(1,4)
        if randomNum < 3:
            mon = monsterpedia.Skeleton()
            monsters.append(mon)
            if randomNum == 1:
                mon = monsterpedia.Skeleton()
                monsters.append(mon)
            else:
                mon = monsterpedia.Slime()
                mon2 = monsterpedia.Slime()
                monsters.extend([mon, mon2])
        else:
            mon = monsterpedia.Slime()
            mon2 = monsterpedia.Slime()
            monsters.extend([mon, mon2])
            if randomNum == 3:
                mon = monsterpedia.Slime()
                monsters.append(mon)
            else:
                mon = monsterpedia.Slime()
                mon2 = monsterpedia.Slime()
                monsters.extend([mon, mon2])
    
    elif player.location == 6:
        randomNum = random.randint(1,3)
        if randomNum == 1:
            mon = monsterpedia.Naga()
            monsters.append(mon)
        elif randomNum == 2:
            mon = monsterpedia.Skeleton()
            mon2 = monsterpedia.Skeleton()
            monsters.extend([mon, mon2])
        if randomNum == 3:
            mon = monsterpedia.Slime()
            mon2 = monsterpedia.Slime()
            mon3 = monsterpedia.Slime()
            mon4 = monsterpedia.Slime()
            mon5 = monsterpedia.Slime()
            monsters.extend([mon, mon2, mon3, mon4, mon5])

    elif player.location == 7:
        randomNum = random.randint(1,4)
        mon = monsterpedia.Goblin()
        monsters.append(mon)
        if randomNum == 1:
            mon = monsterpedia.Goblin()
            mon2 = monsterpedia.GoblinMage()
            monsters.extend([mon, mon2])
        elif randomNum == 2:
            mon = monsterpedia.Skeleton()
            mon2 = monsterpedia.Skeleton()
            monsters.extend([mon, mon2])
        elif randomNum == 3:
            mon = monsterpedia.Naga()
            mon2 = monsterpedia.Goblin()
            monsters.extend([mon, mon2])
        elif randomNum == 4:
            mon = monsterpedia.Goblin()
            monsters.append(mon)
    
    elif player.location == 8:
        randomNum = random.randint(1,4)
        if randomNum == 1:
            mon = monsterpedia.GoblinMage()
            mon2 = monsterpedia.GoblinMage()
            mon3 = monsterpedia.Skeleton()
            monsters.extend([mon, mon2, mon3])
        elif randomNum == 2:
            mon = monsterpedia.Naga()
            mon2 = monsterpedia.Naga()
            monsters.extend([mon, mon2])
        elif randomNum == 3:
            mon = monsterpedia.Naga()
            mon2 = monsterpedia.Goblin()
            mon3 = monsterpedia.Skeleton()
            monsters.extend([mon, mon2, mon3])
        elif randomNum == 4:
            mon = monsterpedia.GoblinMage()
            mon2 = monsterpedia.GoblinMage()
            mon3 = monsterpedia.Goblin()
            monsters.extend([mon, mon2, mon3])

    elif player.location == 9:
        mon = monsterpedia.Slime()
        mon2 = monsterpedia.Skeleton()
        mon3 = monsterpedia.Naga()
        mon4 = monsterpedia.Goblin()
        mon5 = monsterpedia.GoblinMage()
        monsters.extend([mon, mon2, mon3, mon4, mon5])

    elif player.location == 10:
        mon = monsterpedia.GoblinChampion()
        monsters.append(mon)

    else:
        mon = monsterpedia.GoblinChampion()
        monsters.append(mon)
    
    fight(player, monsters)

    # if dead return exit:
    if player.hp == 0:
        return True

    if player.location == 10:
        print("You have defeated the goblin champion, this test game is now over!")
        return True
    
    return False


main()

